package io.nbos.capi.modules.identity.v0;

import io.nbos.capi.api.v0.models.RestMessage;
import io.nbos.capi.api.v0.models.TokenApiModel;
import io.nbos.capi.api.v0.support.IdnCallback;
import io.nbos.capi.api.v0.support.NetworkApi;
import io.nbos.capi.modules.identity.v0.models.LoginModel;
import io.nbos.capi.modules.identity.v0.models.MemberApiModel;
import io.nbos.capi.modules.identity.v0.models.MemberSignupModel;
import io.nbos.capi.modules.identity.v0.models.NewMemberApiModel;
import io.nbos.capi.modules.identity.v0.models.ResetPasswordModel;
import io.nbos.capi.modules.identity.v0.models.SocialConnectApiModel;
import io.nbos.capi.modules.identity.v0.models.SocialConnectUrlResponse;
import io.nbos.capi.modules.identity.v0.models.UpdateMemberApiModel;
import io.nbos.capi.modules.identity.v0.models.UpdatePasswordApiModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class IdentityApi extends NetworkApi {
    public IdentityApi() {
        super();
        setModuleName("identity");
        setRemoteApiClass(IdentityRemoteApi.class);
    }

    public void login(LoginModel loginModel, final IdnCallback<NewMemberApiModel> callback) {

        IdentityRemoteApi identityRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        Call<NewMemberApiModel> call = identityRemoteApi.login("Bearer " + tokenApiModel.getAccess_token(), loginModel);

        call.enqueue(new Callback<NewMemberApiModel>() {
            @Override
            public void onResponse(Call<NewMemberApiModel> call, Response<NewMemberApiModel> response) {
                if (response.code() == 200) {
                    NewMemberApiModel newMemberApiModel = response.body();
                    apiContext.setUserToken(moduleName, newMemberApiModel.getToken());
                }
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<NewMemberApiModel> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void signup(MemberSignupModel memberSignupModel, final IdnCallback<NewMemberApiModel> callback) {
        IdentityRemoteApi identityRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        Call<NewMemberApiModel> call = identityRemoteApi.signup("Bearer " + tokenApiModel.getAccess_token(), memberSignupModel);

        call.enqueue(new Callback<NewMemberApiModel>() {
            @Override
            public void onResponse(Call<NewMemberApiModel> call, Response<NewMemberApiModel> response) {
                if (response.code() == 200) {
                    NewMemberApiModel newMemberApiModel = response.body();
                    apiContext.setUserToken(moduleName, newMemberApiModel.getToken());
                }
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<NewMemberApiModel> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void connect(SocialConnectApiModel socialConnectApiModel, String connectService, final IdnCallback<NewMemberApiModel> callback) {
        IdentityRemoteApi identityRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        Call<NewMemberApiModel> call = identityRemoteApi.connect("Bearer " + tokenApiModel.getAccess_token(), connectService, socialConnectApiModel);
        call.enqueue(new Callback<NewMemberApiModel>() {
            @Override
            public void onResponse(Call<NewMemberApiModel> call, Response<NewMemberApiModel> response) {
                if (response.code() == 200) {
                    NewMemberApiModel newMemberApiModel = response.body();
                    apiContext.setUserToken(moduleName, newMemberApiModel.getToken());
                }
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<NewMemberApiModel> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void authorize(String authorizeService, String code, String state, final IdnCallback<NewMemberApiModel> callback) {
        IdentityRemoteApi identityRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        Call<NewMemberApiModel> call = identityRemoteApi.authorize("Bearer " + tokenApiModel.getAccess_token(), authorizeService, code, state);
        call.enqueue(new Callback<NewMemberApiModel>() {
            @Override
            public void onResponse(Call<NewMemberApiModel> call, Response<NewMemberApiModel> response) {
                if (response.code() == 200) {
                    NewMemberApiModel newMemberApiModel = response.body();
                    apiContext.setUserToken(moduleName, newMemberApiModel.getToken());
                }
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<NewMemberApiModel> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getMemberDetails(String uuid, final IdnCallback<MemberApiModel> callback) {
        IdentityRemoteApi identityRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getUserToken(moduleName);
        Call<MemberApiModel> call = identityRemoteApi.getMemberDetails("Bearer " + tokenApiModel.getAccess_token(), uuid);
        call.enqueue(new Callback<MemberApiModel>() {
            @Override
            public void onResponse(Call<MemberApiModel> call, Response<MemberApiModel> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                }
            }

            @Override
            public void onFailure(Call<MemberApiModel> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void updateMemberDetails(String uuid, UpdateMemberApiModel memberApiModel, final IdnCallback<MemberApiModel> callback) {
        IdentityRemoteApi identityRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getUserToken(moduleName);
        Call<MemberApiModel> call = identityRemoteApi.updateMemberDetails("Bearer " + tokenApiModel.getAccess_token(), uuid, memberApiModel);
        call.enqueue(new Callback<MemberApiModel>() {
            @Override
            public void onResponse(Call<MemberApiModel> call, Response<MemberApiModel> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                }
            }

            @Override
            public void onFailure(Call<MemberApiModel> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void updateCredentials(UpdatePasswordApiModel updatePasswordApiModel, final IdnCallback<RestMessage> callback) {
        IdentityRemoteApi identityRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getUserToken(moduleName);
        Call<RestMessage> call = identityRemoteApi.updateCredentials("Bearer " + tokenApiModel.getAccess_token(), updatePasswordApiModel);
        call.enqueue(new Callback<RestMessage>() {
            @Override
            public void onResponse(Call<RestMessage> call, Response<RestMessage> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                }
            }

            @Override
            public void onFailure(Call<RestMessage> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void resetCredentials(ResetPasswordModel resetPasswordModel, final IdnCallback<RestMessage> callback) {
        IdentityRemoteApi identityRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getUserToken(moduleName);
        Call<RestMessage> call = identityRemoteApi.resetCredentials("Bearer " + tokenApiModel.getAccess_token(), resetPasswordModel);
        call.enqueue(new Callback<RestMessage>() {
            @Override
            public void onResponse(Call<RestMessage> call, Response<RestMessage> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                }
            }

            @Override
            public void onFailure(Call<RestMessage> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void logout(final IdnCallback<RestMessage> callback) {
        IdentityRemoteApi identityRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getUserToken(moduleName);
        Call<RestMessage> call = identityRemoteApi.logout("Bearer " + tokenApiModel.getAccess_token());
        RestMessage member = null;
        call.enqueue(new Callback<RestMessage>() {
            @Override
            public void onResponse(Call<RestMessage> call, Response<RestMessage> response) {
                if (response.code() == 200) {
                    apiContext.setUserToken(moduleName, null);
                    callback.onResponse(response);
                }
            }

            @Override
            public void onFailure(Call<RestMessage> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }


    public void socialWebViewConnect(String connectService, final IdnCallback<SocialConnectUrlResponse> callback) {
        IdentityRemoteApi identityRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getUserToken(moduleName);
        Call<SocialConnectUrlResponse> call = identityRemoteApi.socialWebViewLogin("Bearer " + tokenApiModel.getAccess_token(), connectService);
        call.enqueue(new Callback<SocialConnectUrlResponse>() {
            @Override
            public void onResponse(Call<SocialConnectUrlResponse> call, Response<SocialConnectUrlResponse> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                }
            }

            @Override
            public void onFailure(Call<SocialConnectUrlResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

}
