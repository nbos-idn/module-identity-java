package io.nbos.capi.modules.identity.v0.models;

public class UpdateMemberApiModel {
    private String email;
    private String firstName;
    private String lastName;
    private String phone;
    private String description;


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setEmail(String email) {
        this.email = email;
    }

}
